package com.lpiem.ran.java;

import com.lpiem.ran.java.model.Card;
import com.lpiem.ran.java.model.CardType;
import com.lpiem.ran.java.model.pokemon.IFight;
import com.lpiem.ran.java.model.pokemon.struct.EnergyType;
import com.lpiem.ran.java.model.pokemon.Pokemon;
import com.lpiem.ran.java.utils.NoOrderException;

import java.util.ArrayList;
import java.util.List;

public class CardTpRun {

    public List<Card> createCardList() {
        List<Card> cardList = new ArrayList<Card>();
        cardList.add(
                new Pokemon("Toto", "previewUrl", CardType.POKEMON, EnergyType.FIRE)
        );

        cardList.add(
                new Pokemon("Pikachu", "previewUrlPika", CardType.POKEMON, EnergyType.THUNDER)
        );

        return cardList;
    }

    public void logCards(List<Card> listCard) {
        for (Card card : listCard) {
            System.out.println(card.toString());
        }
    }


    public void logCards2(List<Card> listCard) {
        for (Card card : listCard) {
            System.out.println(card.toString());
            if(card instanceof IFight) {
                ((IFight) card).fight();
            }
        }
    }


    public List<Card> orderBy(List<Card> cards) throws NoOrderException {
        List<Card> filterCards = new ArrayList<>();

        if (cards == null) {
            throw new NoOrderException("Liste NULL");
        }

        if (cards.isEmpty()) {
            throw new NoOrderException("Liste vide");
        }



        return filterCards;
    }

    public List<Card> filterByType(List<Card> cards, CardType type) {
        List<Card> filterCards = new ArrayList<>();

        for (Card card : cards) {
            if (card.getType() == type) {
                filterCards.add(card);
            }
        }

        return filterCards;

    }
}
