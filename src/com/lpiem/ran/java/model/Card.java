package com.lpiem.ran.java.model;


public abstract class Card {

    private String name;
    private String urlPreview;
    private CardType type;
    public int typeInt = 0;
    public Integer superType = 1;

    public Card(String name) {
        this.name = name;
    }

    public Card(String name, String urlPreview, CardType type) {
        this.name = name;
        this.urlPreview = urlPreview;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlPreview() {
        return urlPreview;
    }

    public void setUrlPreview(String urlPreview) {
        this.urlPreview = urlPreview;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Card{" +
                "name='" + name + '\'' +
                ", urlPreview='" + urlPreview + '\'' +
                ", type=" + type +
                ", typeInt=" + typeInt +
                ", superType=" + superType +
                '}';
    }
}
