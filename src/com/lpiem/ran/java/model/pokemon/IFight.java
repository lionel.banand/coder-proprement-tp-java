package com.lpiem.ran.java.model.pokemon;

public interface IFight {

    public int getForce();
    public int getDefense();
    public Boolean fight();

}
