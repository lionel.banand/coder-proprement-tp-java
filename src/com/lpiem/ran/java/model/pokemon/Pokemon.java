package com.lpiem.ran.java.model.pokemon;

import com.lpiem.ran.java.model.Card;
import com.lpiem.ran.java.model.CardType;
import com.lpiem.ran.java.model.pokemon.struct.EnergyType;

public class Pokemon extends Card implements IFight {
    private EnergyType energy;

    private int attack = 0;

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public EnergyType getEnergy() {
        return energy;
    }

    public void setEnergy(EnergyType energy) {
        this.energy = energy;
    }



    public Pokemon(String name, String urlPreview, CardType type, EnergyType energy) {
        super(name, urlPreview, type);
        this.energy = energy;
    }

    @Override
    public String toString() {

        String cardSrting = super.toString();
        return cardSrting + "Pokemon{" +
                "energy=" + energy +
                '}';
    }

    @Override
    public int getForce() {
        return 10;
    }

    @Override
    public int getDefense() {
        return 0;
    }

    @Override
    public Boolean fight() {
        return getForce()  > getDefense();
    }
}
