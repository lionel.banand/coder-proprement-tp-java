package com.lpiem.ran.java.model.pokemon.struct;

public enum EnergyType {
    FIRE,
    WATER,
    EARTH,
    THUNDER
}
