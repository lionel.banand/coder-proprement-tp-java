package com.lpiem.ran.java.utils;

import com.lpiem.ran.java.model.Card;
import com.lpiem.ran.java.model.CardType;
import com.lpiem.ran.java.model.pokemon.Pokemon;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Tools {

    private static Tools instance = null;

    public static Tools getInstance() {
        if (instance == null) {
            instance = new Tools();
        }
        return instance;
    }

    private Tools() {}

    public ArrayList<Card> sortCardsByType(List<Card> cards)  {

        ArrayList<Card> sortedCartes = new ArrayList<Card>(cards);

        sortedCartes.sort(Comparator.comparing(Card::getType));

        return sortedCartes;
    }

    public ArrayList<Pokemon> sortAscAttack(List<Card> cards) {

        ArrayList<Pokemon> sortedCartes = new ArrayList<>();

        for (Card card : cards) {
            if(card.getType() == CardType.POKEMON) {
                sortedCartes.add((Pokemon) card);
            }
        }


        try {
            for (int i = 0; i < sortedCartes.size() - 1; i++) {
                for (int j = sortedCartes.size() - 1; j > i; j--) {
                    if (sortedCartes.get(j - 1).getAttack() > sortedCartes.get(j).getAttack()) {
                        Pokemon card = sortedCartes.get(j - 1);
                        sortedCartes.set(j - 1, sortedCartes.get(j));
                        sortedCartes.set(j, card);
                    }
                }
            }
        }
        catch (Exception e ){
            System.out.println(e);
        }

        return sortedCartes;
    }
}
