package com.lpiem.ran.java.utils;

public class NoOrderException extends Exception {
    public NoOrderException(String message) {
        super(message);
    }
}
