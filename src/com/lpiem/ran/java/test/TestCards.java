package com.lpiem.ran.java.test;

import com.lpiem.ran.java.CardTpRun;
import com.lpiem.ran.java.model.Card;
import com.lpiem.ran.java.utils.NoOrderException;

import java.util.ArrayList;
import java.util.List;

public class TestCards {

    public void testOrderBy() {
        CardTpRun tp = new CardTpRun();

        List<Card> listNull = null;
        List<Card> listEmpty = new ArrayList<>();

        try {
            listNull = tp.orderBy(null);
        } catch (NoOrderException e) {
            System.out.println(e.getMessage());
        }

        try {
            listEmpty = tp.orderBy(new ArrayList<>());
        } catch (Error error) {
            System.out.println(error);
        } catch (NoOrderException e) {
            System.out.println(e);
        }

        if (listNull == null) {
            System.out.println("ERROR LIST NULL");
        } else if(listEmpty.size() != 0) {
            System.out.println("ERROR LIST Empty is not empty");
        }

        else {
            System.out.println("LIST ORDER OK");
        }
    }

}
