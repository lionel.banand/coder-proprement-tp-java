package com.lpiem.ran.java;

import com.lpiem.ran.java.model.Card;
import com.lpiem.ran.java.model.CardType;
import com.lpiem.ran.java.test.TestCards;
import com.lpiem.ran.java.utils.NoOrderException;
import com.lpiem.ran.java.utils.Tools;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here

        CardTpRun tp = new CardTpRun();
        //Creation des cartes pour le test
        List<Card> cards = tp.createCardList();
        //log des cartes pour le test
        tp.logCards(cards);

        //recup instace Tools
        Tools tool = Tools.getInstance();
        //trie des cartes générées pour le test
        List<Card> listTool = tool.sortCardsByType(cards);
        //log les cartes triées
        tp.logCards(listTool);

        Tools toto = Tools.getInstance();

        //tp.logCards(Tools.getInstance().sortCardsByType(cards));

        List<Card> cardsToLog = new ArrayList<>();
        try {
            cardsToLog = tp.orderBy(new ArrayList<>());
        } catch (NoOrderException e) {
            System.out.println(e.getMessage());
            cardsToLog = cards;
        }

        System.out.println("LOg apres exception");
        tp.logCards(cardsToLog);

        tests();


    }

    public static void tests() {
        TestCards t = new TestCards();
        t.testOrderBy();
    }

}
